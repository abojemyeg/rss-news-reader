package rss_news_reader.a7md.com.rssnewsreader.db.dao.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import rss_news_reader.a7md.com.rssnewsreader.db.dao.DBTable;
import rss_news_reader.a7md.com.rssnewsreader.db.domain.Article;

public class Articles extends DBTable<Article> {

    private static final String COLUMN_NAME_IMAGE_LINK = "image_link";
    private static final String COLUMN_NAME_LINK = "link";
    private static final String COLUMN_NAME_PROVIDER_ID = "provider_id";
    private static final String COLUMN_NAME_DESCRIPTION = "description";

    String[] cols = {
            COLUMN_NAME_ID,
            COLUMN_NAME_TITLE,
            COLUMN_NAME_LINK,
            COLUMN_NAME_PROVIDER_ID,
            COLUMN_NAME_IMAGE_LINK,
            COLUMN_NAME_DESCRIPTION
    };

    public Articles() {
        super("Articles");
    }

    @Override
    protected String getCreateStatement() {
        return "CREATE TABLE " + getTableName() + " (" +
                COLUMN_NAME_ID + " INTEGER PRIMARY KEY," +
                COLUMN_NAME_TITLE + " TEXT," +
                COLUMN_NAME_DESCRIPTION + " TEXT," +
                COLUMN_NAME_LINK + " TEXT," +
                COLUMN_NAME_PROVIDER_ID + " INTEGER," +
                COLUMN_NAME_IMAGE_LINK + " TEXT)";
    }

    @Override
    public ArrayList<Article> getList() {
        SQLiteDatabase db = getDbHelper().getReadableDatabase();

        Cursor cursor = db.query(getTableName(),
                cols,
                null,
                null,
                null,
                null,
                null
        );
        ArrayList<Article> list = new ArrayList<>();

        while (cursor.moveToNext()) {
            list.add(fromCursor(cursor));
        }
        return list;
    }

    public ArrayList<Article> getListOfProvider(long provider_id) {
        SQLiteDatabase db = getDbHelper().getReadableDatabase();

        Cursor cursor = db.query(getTableName(),
                cols,
                COLUMN_NAME_PROVIDER_ID + "=?",
                new String[]{provider_id + ""},
                null,
                null,
                null
        );
        ArrayList<Article> list = new ArrayList<>();

        while (cursor.moveToNext()) {
            list.add(fromCursor(cursor));
        }
        return list;
    }

    @Override
    public Article getItemById(long id) {
        SQLiteDatabase db = getDbHelper().getReadableDatabase();

        String selection = COLUMN_NAME_ID + "=?";
        String[] selectionArgs = {id + ""};


        Cursor cursor = db.query(getTableName(),
                cols,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        if (cursor.moveToNext()) {
            return fromCursor(cursor);
        }
        return null;
    }


    @Override
    public void deleteById(long id) {
        getDbHelper().getWritableDatabase().delete(getTableName(), COLUMN_NAME_ID + "=?", new String[]{id + ""});
    }

    @Override
    public void addItem(Article article) {
        SQLiteDatabase db = getDbHelper().getWritableDatabase();
        ContentValues values = itemToRow(article);
        long newRowId = db.insert(getTableName(), null, values);
        article.setId(newRowId);
    }

    @Override
    public void updateItem(Article article) {
        SQLiteDatabase db = getDbHelper().getWritableDatabase();
        ContentValues values = itemToRow(article);
        String selection = COLUMN_NAME_ID + "=?";
        String[] selectionArgs = {article.getId() + ""};

        db.update(getTableName(),
                values,
                selection,
                selectionArgs);
    }

    @Override
    protected Article fromCursor(Cursor cursor) {
        Article article = new Article();
        article.setId(cursor.getLong(cursor.getColumnIndex(COLUMN_NAME_ID)));
        article.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_NAME_TITLE)));
        article.setImgLink(cursor.getString(cursor.getColumnIndex(COLUMN_NAME_IMAGE_LINK)));
        article.setLink(cursor.getString(cursor.getColumnIndex(COLUMN_NAME_LINK)));
        article.setProviderId(cursor.getLong(cursor.getColumnIndex(COLUMN_NAME_PROVIDER_ID)));
        article.setDescription(cursor.getString(cursor.getColumnIndex(COLUMN_NAME_DESCRIPTION)));
        System.out.println(article);
        return article;
    }

    @Override
    protected ContentValues itemToRow(Article article) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_TITLE, article.getTitle());
        values.put(COLUMN_NAME_IMAGE_LINK, article.getImgLink());
        values.put(COLUMN_NAME_LINK, article.getLink());
        values.put(COLUMN_NAME_PROVIDER_ID, article.getProviderId());
        values.put(COLUMN_NAME_DESCRIPTION, article.getDescription());
        return values;
    }

    public void deleteByProviderId(long provider_id) {
        String selection = COLUMN_NAME_PROVIDER_ID + "=?";
        String[] selectionArgs = {provider_id + ""};
        getDbHelper().getWritableDatabase().delete(getTableName(), selection, selectionArgs);
    }
}
