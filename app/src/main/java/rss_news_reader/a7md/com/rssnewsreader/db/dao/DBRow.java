package rss_news_reader.a7md.com.rssnewsreader.db.dao;

public interface DBRow {
    long getId();

    String getTitle();
}
