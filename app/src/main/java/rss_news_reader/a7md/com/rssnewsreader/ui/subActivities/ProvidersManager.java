package rss_news_reader.a7md.com.rssnewsreader.ui.subActivities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.util.Consumer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.mcsoxford.rss.RSSFeed;
import org.mcsoxford.rss.RSSReader;

import rss_news_reader.a7md.com.rssnewsreader.R;
import rss_news_reader.a7md.com.rssnewsreader.db.dao.DbHelper;
import rss_news_reader.a7md.com.rssnewsreader.db.dao.entities.Providers;
import rss_news_reader.a7md.com.rssnewsreader.db.domain.Provider;

public class ProvidersManager extends AppCompatActivity {

    Providers providers = new Providers();
    ListView listView;
    EditText titleEditText, rss_urlEditText;
    CheckBox autoFetchTitle;
    Button add_button;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_providers_manager);

        listView = findViewById(R.id.list_view);
        titleEditText = findViewById(R.id.title_text_edit);
        rss_urlEditText = findViewById(R.id.rss_url_text_edit);
        add_button = findViewById(R.id.add_button);
        autoFetchTitle = findViewById(R.id.auto_fetch);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        autoFetchTitle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                titleEditText.setEnabled(!isChecked);
            }
        });

        new DbHelper(this, providers);

        add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewProvider();
            }
        });
        listView.setAdapter(new ListAdapter());
    }


    private void addNewProvider() {
        progressBar.setVisibility(View.VISIBLE);
        final Exception[] e = new Exception[1];

        Consumer<String> onDone = new Consumer<String>() {/// onDone accept String
            @Override
            public void accept(String title) {
                progressBar.setVisibility(View.GONE);
                if (e[0] != null) {
                    return;
                }
                if (!autoFetchTitle.isChecked()) {
                    title = titleEditText.getText().toString();
                }
                String url = rss_urlEditText.getText().toString();

                Provider provider = new Provider();
                provider.setActive(true);
                provider.setLink(url);
                provider.setTitle(title);

                providers.addItem(provider);
                refresh();
            }
        };

        Consumer<Exception> onFail = new Consumer<Exception>() {/// onFail accept Exception
            @Override
            public void accept(final Exception error) {
                ProvidersManager.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (error instanceof IllegalStateException) {
                            Toast.makeText(ProvidersManager.this, "Please make sure that this is a valid rss url", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(ProvidersManager.this, error.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
                error.printStackTrace();
                e[0] = error;
            }
        };

        new checkRssLink(onDone, onFail).execute(rss_urlEditText.getText().toString());
    }

    private void refresh() {
        listView.setAdapter(new ListAdapter());

        titleEditText.setText("");
        rss_urlEditText.setText("");
        autoFetchTitle.setChecked(false);
        titleEditText.setEnabled(true);

        progressBar.setVisibility(View.GONE);
    }

    class ListAdapter extends BaseAdapter {
        final Provider[] currentList;

        ListAdapter() {
            currentList = providers.getList().toArray(new Provider[0]);
        }

        @Override
        public int getCount() {
            return currentList.length;
        }

        @Override
        public Object getItem(int position) {
            return currentList[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final Provider current_provider = currentList[position];

            View view = getLayoutInflater().inflate(R.layout.manage_providers_list_item, null);

            TextView title = view.findViewById(R.id.title);
            TextView link = view.findViewById(R.id.link);
            Switch activate_switch = view.findViewById(R.id.activate_switch);
            ImageButton del = view.findViewById(R.id.delete_button);

            title.setText(current_provider.getTitle());
            link.setText(current_provider.getLink());
            activate_switch.setChecked(current_provider.isActive());

            activate_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    current_provider.setActive(isChecked);
                    providers.updateItem(current_provider);
                    listView.setAdapter(new ListAdapter());
                }
            });
            del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    providers.deleteById(current_provider.getId());
                    listView.setAdapter(new ListAdapter());
                }
            });

            return view;
        }
    }

    private static class checkRssLink extends AsyncTask<String, Integer, String> {

        private Consumer<String> onDone;
        private Consumer<Exception> onFail;

        checkRssLink(Consumer<String> onDone, Consumer<Exception> onFail) {
            this.onDone = onDone;
            this.onFail = onFail;
        }

        @Override
        protected String doInBackground(String... strings) {
            String title = null;
            try {
                RSSReader reader = new RSSReader();
                RSSFeed feed = reader.load(strings[0]);
                title = feed.getTitle();
                if (title == null) {
                    title = "";
                }
            } catch (Exception e) {
                onFail.accept(e);
            }
            return title;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            onDone.accept(s);
        }
    }
}
