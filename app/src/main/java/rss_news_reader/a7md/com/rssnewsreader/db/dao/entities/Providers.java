package rss_news_reader.a7md.com.rssnewsreader.db.dao.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import rss_news_reader.a7md.com.rssnewsreader.db.dao.DBTable;
import rss_news_reader.a7md.com.rssnewsreader.db.domain.Provider;

public class Providers extends DBTable<Provider> {

    private static final String COLUMN_NAME_LINK = "link";
    private static final String COLUMN_NAME_ACTIVE = "active";

    String[] cols = {
            COLUMN_NAME_ID,
            COLUMN_NAME_TITLE,
            COLUMN_NAME_ACTIVE,
            COLUMN_NAME_LINK
    };

    public Providers() {
        super("Providers");
    }

    @Override
    protected void onCreate(SQLiteDatabase db) {
        super.onCreate(db);

        Provider provider = new Provider();
        provider.setActive(true);

        /// Adding default providers
        provider.setLink("https://www.businessinsider.com/rss");
        provider.setTitle("Business Insider");
        db.insert(getTableName(), null, itemToRow(provider));

        provider.setLink("https://gizmodo.com/rss");
        provider.setTitle("Gizmodo");
        db.insert(getTableName(), null, itemToRow(provider));


        provider.setLink("https://mashable.com/rss/");
        provider.setTitle("Mashable");
        db.insert(getTableName(), null, itemToRow(provider));
    }

    @Override
    protected String getCreateStatement() {
        return "CREATE TABLE " + getTableName() + " (" +
                COLUMN_NAME_ID + " INTEGER PRIMARY KEY," +
                COLUMN_NAME_TITLE + " TEXT," +
                COLUMN_NAME_ACTIVE + " TEXT," +
                COLUMN_NAME_LINK + " TEXT)";
    }

    @Override
    public ArrayList<Provider> getList() {
        SQLiteDatabase db = getDbHelper().getReadableDatabase();
        Cursor cursor = db.query(getTableName(),
                cols,
                null,
                null,
                null,
                null,
                null
        );
        ArrayList<Provider> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            list.add(fromCursor(cursor));
        }
        return list;
    }


    public Provider[] getActiveList() {
        SQLiteDatabase db = getDbHelper().getReadableDatabase();
        String selection = COLUMN_NAME_ACTIVE + "=?";
        String[] selectionArgs = {"1"};

        Cursor cursor = db.query(getTableName(),
                cols,
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        ArrayList<Provider> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            Provider provider = fromCursor(cursor);
            list.add(provider);
        }
        return list.toArray(new Provider[0]);
    }

    @Override
    public Provider getItemById(long id) {
        SQLiteDatabase db = getDbHelper().getReadableDatabase();

        String selection = COLUMN_NAME_ID + "=?";
        String[] selectionArgs = {id + ""};


        Cursor cursor = db.query(getTableName(),
                cols,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        if (cursor.moveToNext()) {
            return fromCursor(cursor);
        }
        return null;
    }


    @Override
    public void deleteById(long id) {
        getDbHelper().getWritableDatabase().delete(getTableName(), COLUMN_NAME_ID + "=?", new String[]{id + ""});
    }

    @Override
    public void addItem(Provider provider) {
        SQLiteDatabase db = getDbHelper().getWritableDatabase();

        ContentValues values = itemToRow(provider);

        long newRowId = db.insert(getTableName(), null, values);
        provider.setId(newRowId);
    }

    @Override
    public void updateItem(Provider provider) {
        SQLiteDatabase db = getDbHelper().getWritableDatabase();

        ContentValues values = itemToRow(provider);

        String selection = COLUMN_NAME_ID + "=?";
        String[] selectionArgs = {provider.getId() + ""};

        db.update(
                getTableName(),
                values,
                selection,
                selectionArgs);
    }

    @Override
    protected Provider fromCursor(Cursor cursor) {
        Provider article = new Provider();

        article.setId(cursor.getLong(cursor.getColumnIndex(COLUMN_NAME_ID)));
        article.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_NAME_TITLE)));
        article.setLink(cursor.getString(cursor.getColumnIndex(COLUMN_NAME_LINK)));
        int isActiveVal = cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_ACTIVE));

        article.setActive(isActiveVal != 0);

        return article;
    }

    @Override
    protected ContentValues itemToRow(Provider provider) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_TITLE, provider.getTitle());
        values.put(COLUMN_NAME_LINK, provider.getLink());
        values.put(COLUMN_NAME_ACTIVE, provider.isActive() ? 1 : 0);
        return values;
    }
}
