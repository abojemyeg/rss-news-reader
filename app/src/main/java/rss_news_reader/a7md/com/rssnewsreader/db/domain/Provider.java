package rss_news_reader.a7md.com.rssnewsreader.db.domain;

import rss_news_reader.a7md.com.rssnewsreader.db.dao.DBRow;

public class Provider implements DBRow {
    long id;
    String title;
    String link;
    boolean active;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getLink() {
        return link;
    }


    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
