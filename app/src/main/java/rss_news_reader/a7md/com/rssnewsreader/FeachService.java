package rss_news_reader.a7md.com.rssnewsreader;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.util.Consumer;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import rss_news_reader.a7md.com.rssnewsreader.db.dao.DbHelper;
import rss_news_reader.a7md.com.rssnewsreader.db.dao.entities.Articles;
import rss_news_reader.a7md.com.rssnewsreader.db.dao.entities.Providers;
import rss_news_reader.a7md.com.rssnewsreader.processor.RssURLParser;

import static rss_news_reader.a7md.com.rssnewsreader.processor.RssURLParser.isInternetAvailable;

public class FeachService extends Service {
    public static final int notify = 600000;  //interval between two services(Here Service run every 10 Minute)
    private Handler mHandler = new Handler();   //run on another Thread to avoid crash
    private Timer mTimer = null;    //timer handling

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        if (mTimer != null) // Cancel if already existed
            mTimer.cancel();
        else
            mTimer = new Timer();   //recreate new
        mTimer.scheduleAtFixedRate(new TimeDisplay(), notify, notify);   //Schedule task
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTimer.cancel();    //For Cancel Timer
        Toast.makeText(this, "Service is Destroyed", Toast.LENGTH_SHORT).show();
    }

    //class TimeDisplay for handling task
    class TimeDisplay extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    refreshSSs();
                }
            });
        }

        private void refreshSSs() {
            try {
                if (!isInternetAvailable()) {
                    throw new Exception("please check the internet connection");
                }
                Articles articles = new Articles();
                Providers providers = new Providers();
                new DbHelper(FeachService.this, articles, providers);

                Runnable onFinish = new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(FeachService.this, "done fetching news", Toast.LENGTH_SHORT).show();
                    }
                };
                new RssURLParser(providers, articles, new Consumer<Exception>() {
                    @Override
                    public void accept(Exception e) {
                        Toast.makeText(FeachService.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, onFinish);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
