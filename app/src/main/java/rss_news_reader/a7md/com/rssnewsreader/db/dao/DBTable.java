package rss_news_reader.a7md.com.rssnewsreader.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

//// TODO: 1/11/2019 convert this class to interface
public abstract class DBTable<Item extends DBRow> {
    final String tableName;
    private DbHelper dbHelper;

    protected static final String COLUMN_NAME_ID = "id";
    protected static final String COLUMN_NAME_TITLE = "title";

    protected DBTable(String tableName) {
        this.tableName = tableName;
    }

    public DbHelper getDbHelper() {
        return dbHelper;
    }

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public String getTableName() {
        return tableName;
    }

    abstract protected String getCreateStatement();

    abstract public ArrayList<Item> getList();

    abstract public Item getItemById(long id);

    abstract public void deleteById(long id);

    abstract public void addItem(Item item);

    abstract public void updateItem(Item item);

    abstract protected Item fromCursor(Cursor cursor);

    abstract protected ContentValues itemToRow(Item cursor);

    protected void onCreate(SQLiteDatabase db) {
        System.out.println("creating table " + tableName + " ...");
    }
}