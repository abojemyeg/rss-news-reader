package rss_news_reader.a7md.com.rssnewsreader;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.util.Consumer;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;
import android.widget.Toast;

import rss_news_reader.a7md.com.rssnewsreader.db.dao.DbHelper;
import rss_news_reader.a7md.com.rssnewsreader.db.dao.entities.Articles;
import rss_news_reader.a7md.com.rssnewsreader.db.dao.entities.Providers;
import rss_news_reader.a7md.com.rssnewsreader.processor.RssURLParser;
import rss_news_reader.a7md.com.rssnewsreader.ui.ListAdapter;
import rss_news_reader.a7md.com.rssnewsreader.ui.subActivities.ProvidersManager;

public class MainActivity extends AppCompatActivity {

    private Articles articles = new Articles();
    private Providers providers = new Providers();
    public ExpandableListView expandableListView;
    private ListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new DbHelper(this, articles, providers);
        refreshSSs();
        initList();
        startService(new Intent(this, FeachService.class)); //start service which fetching the news
    }

    private void refreshSSs() {
        try {
            Runnable onFinish = new Runnable() {
                @Override
                public void run() {
                    listAdapter.refresh();
                }
            };
            new RssURLParser(providers, articles, new Consumer<Exception>() {
                @Override
                public void accept(Exception e) {
                    showMsg(e.getMessage());
                }
            }, onFinish);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initList() {
        listAdapter = new ListAdapter(this);
        this.expandableListView = findViewById(R.id.expandableListView);
        expandableListView.setAdapter(listAdapter);

    }

    public void showMsg(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    public Articles getArticles() {
        return articles;
    }

    public Providers getProviders() {
        return providers;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(getResources().getString(R.string.manage_providers)).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(getBaseContext(), ProvidersManager.class);
                startActivity(intent);
                return false;
            }
        });

        menu.add("refresh").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                refreshSSs();
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshSSs();
        listAdapter.refresh();
    }
}
