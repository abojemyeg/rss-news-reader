package rss_news_reader.a7md.com.rssnewsreader.processor;

import android.os.AsyncTask;
import android.support.v4.util.Consumer;

import org.mcsoxford.rss.RSSFeed;
import org.mcsoxford.rss.RSSItem;
import org.mcsoxford.rss.RSSReader;
import org.mcsoxford.rss.RSSReaderException;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import rss_news_reader.a7md.com.rssnewsreader.db.dao.entities.Articles;
import rss_news_reader.a7md.com.rssnewsreader.db.dao.entities.Providers;
import rss_news_reader.a7md.com.rssnewsreader.db.domain.Article;
import rss_news_reader.a7md.com.rssnewsreader.db.domain.Provider;

public class RssURLParser extends AsyncTask<Provider, Integer, Void> {

    private Articles articles;
    private Consumer<Exception> onError;
    private Runnable onFinish;

    public RssURLParser(Providers providers, Articles articles, Consumer<Exception> onError, Runnable onFinish) {
        this.articles = articles;
        this.onError = onError;
        this.onFinish = onFinish;
        Provider[] list = providers.getActiveList();
        this.execute(list);
    }

    @Override
    protected Void doInBackground(Provider... rssCollections) {
    if (!isInternetAvailable()) {
            onError.accept(new Exception("please check the internet connection"));
            return null;
        }
        for (Provider rssCollection : rssCollections) {
            try {
                parseRss(rssCollection);
            } catch (RSSReaderException e) {
                onError.accept(e);
            }
        }
        return null;
    }

    public void parseRss(Provider provider) throws RSSReaderException {
        RSSReader reader = new RSSReader();
        RSSFeed feed = reader.load(provider.getLink());
        ArrayList<Article> new_articles = new ArrayList<>();

        List<RSSItem> items = feed.getItems();
        for (RSSItem item : items) {
            Article article = new Article();

            article.setProviderId(provider.getId());
            article.setTitle(item.getTitle());
            article.setLink(item.getLink().toString());
            article.setDescription(item.getDescription());

            if (!item.getThumbnails().isEmpty()) {
                article.setImgLink(item.getThumbnails().get(0).getUrl().toString());
            } else {
                article.setImgLink("");
            }

            new_articles.add(article);
        }
        reader.close();

        articles.deleteByProviderId(provider.getId());

        for (Article article : new_articles) {
            articles.addItem(article);
        }
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (onFinish != null) onFinish.run();
    }

    static public boolean isInternetAvailable() {
        try {
            final InetAddress address = InetAddress.getByName("www.google.com");
            return !address.equals("");
        } catch (UnknownHostException e) {
            // Log error
        }
        return false;
    }
}
