package rss_news_reader.a7md.com.rssnewsreader.ui;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import rss_news_reader.a7md.com.rssnewsreader.R;
import rss_news_reader.a7md.com.rssnewsreader.db.domain.Article;
import rss_news_reader.a7md.com.rssnewsreader.ui.subActivities.ArticleViewer;

public class ArticleView implements node {

    private View view;
    private ImageView image;
    private TextView title;
    Activity mActivity;

    public ArticleView(Activity activity) {
        view = activity.getLayoutInflater().inflate(R.layout.article_view, null);
        image = view.findViewById(R.id.article_image);
        title = view.findViewById(R.id.article_title);
        this.mActivity = activity;
    }

    public void setArticle(final Article article) {
        this.title.setText(article.getTitle());
        if (!article.getImgLink().isEmpty()) {
            Picasso.get().load(article.getImgLink()).into(image);
        }
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity.getBaseContext(), ArticleViewer.class);
                intent.putExtra(ArticleViewer.EXTRA_ARTICLE_ID, article.getId());
                mActivity.startActivity(intent);
            }
        });
    }

    @Override
    public View getView() {
        return view;
    }
}
