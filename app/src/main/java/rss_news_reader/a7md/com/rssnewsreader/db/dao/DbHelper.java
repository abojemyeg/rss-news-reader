package rss_news_reader.a7md.com.rssnewsreader.db.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "dbmm";
    private DBTable[] list;

    public DbHelper(Context context, DBTable... list) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.list = list;
        for (DBTable dbTable : list) {
            dbTable.setDbHelper(this);
        }

    }

    public void onCreate(SQLiteDatabase db) {
        for (DBTable dbTable : list) {
            db.execSQL(dbTable.getCreateStatement() + ";");
            dbTable.onCreate(db);
        }
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over

        for (DBTable dbTable : list) {
            db.execSQL("DROP TABLE IF EXISTS " + dbTable.tableName + ";");
        }
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}

