package rss_news_reader.a7md.com.rssnewsreader.ui.subActivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Xml;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import rss_news_reader.a7md.com.rssnewsreader.R;
import rss_news_reader.a7md.com.rssnewsreader.db.dao.DbHelper;
import rss_news_reader.a7md.com.rssnewsreader.db.dao.entities.Articles;
import rss_news_reader.a7md.com.rssnewsreader.db.dao.entities.Providers;
import rss_news_reader.a7md.com.rssnewsreader.db.domain.Article;

public class ArticleViewer extends AppCompatActivity {

    public static String EXTRA_ARTICLE_ID = "EXTRA_ARTICLE_ID";

//    private ImageView image;
    private TextView title;
    private TextView link;
    private WebView description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_viewer);

        title = findViewById(R.id.article_title);
        link = findViewById(R.id.article_link);
//        image = findViewById(R.id.article_image);
        description = findViewById(R.id.description);

        long aId = getIntent().getLongExtra(EXTRA_ARTICLE_ID, -1);
        if (aId != -1) {
            Articles a = new Articles();
            Providers p = new Providers();
            DbHelper w = new DbHelper(this, a, p);

            Article itemById = a.getItemById(aId);

            if (itemById != null) {
                setArticle(itemById);
                setTitle(p.getItemById(itemById.getProviderId()).getTitle());
            } else {
                finish();
            }
        }
    }

    public void setArticle(final Article article) {
        this.title.setText(article.getTitle());
        this.link.setText(article.getLink());
        this.description.loadData(article.getDescription(), "text/html", Xml.Encoding.UTF_8.name());
//        if (!article.getImgLink().isEmpty()) {
//            Picasso.get().load(article.getImgLink()).into(image);
//        }
        this.link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), ArticlePageViewer.class);
                intent.putExtra(ArticlePageViewer.EXTRA_ARTICLE_LINK, article.getLink());
                startActivity(intent);
            }
        });
    }
}
