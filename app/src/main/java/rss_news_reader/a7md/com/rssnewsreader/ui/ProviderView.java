package rss_news_reader.a7md.com.rssnewsreader.ui;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import rss_news_reader.a7md.com.rssnewsreader.R;
import rss_news_reader.a7md.com.rssnewsreader.db.domain.Provider;

public class ProviderView implements node {
    private View view;
    private TextView title;
    private TextView link;

    public ProviderView(Activity activity) {
        this.view = activity.getLayoutInflater().inflate(R.layout.provider_view, null);
        this.title = view.findViewById(R.id.title);
        this.link = view.findViewById(R.id.link);
    }

    public void setProvider(Provider provider) {
        title.setText(provider.getTitle());
        link.setText(provider.getLink());
    }

    @Override
    public View getView() {
        return view;
    }
}
