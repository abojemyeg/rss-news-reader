package rss_news_reader.a7md.com.rssnewsreader.ui.subActivities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import rss_news_reader.a7md.com.rssnewsreader.R;

public class ArticlePageViewer extends AppCompatActivity {

    public static String EXTRA_ARTICLE_LINK = "EXTRA_ARTICLE_LINK";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_page_viewer);

        String link = getIntent().getStringExtra(EXTRA_ARTICLE_LINK);
        WebView s = findViewById(R.id.web_view);
        s.loadUrl(link);
    }
}
