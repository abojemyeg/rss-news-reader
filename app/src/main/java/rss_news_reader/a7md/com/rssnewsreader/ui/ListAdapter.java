package rss_news_reader.a7md.com.rssnewsreader.ui;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import java.util.ArrayList;

import rss_news_reader.a7md.com.rssnewsreader.MainActivity;
import rss_news_reader.a7md.com.rssnewsreader.db.domain.Article;
import rss_news_reader.a7md.com.rssnewsreader.db.domain.Provider;

public class ListAdapter extends BaseExpandableListAdapter {

    private MainActivity mainActivity;
    private Provider[] active_providers;
    private ArrayList<Article>[] articles_list;

    public ListAdapter(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        refresh();
    }

    public void refresh() {
        active_providers = mainActivity.getProviders().getActiveList();
        articles_list = new ArrayList[active_providers.length];
        mainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getGroupCount() {
        return articles_list.length;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<Article> articles = getArticles(groupPosition);
        return articles.size();
    }

    //information theory
    public ArrayList<Article> getArticles(int group_index) {
        if (articles_list[group_index] == null) {
            long provider_id = active_providers[group_index].getId();
            articles_list[group_index] = mainActivity.getArticles().getListOfProvider(provider_id);
        }
        return articles_list[group_index];
    }

    @Override
    public Object getGroup(int groupPosition) {
        return active_providers[groupPosition];
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return getArticles(groupPosition).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return groupPosition + 100 + childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ProviderView provider_node = new ProviderView(mainActivity);
        provider_node.setProvider(active_providers[groupPosition]);
        return provider_node.getView();
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ArticleView articleView = new ArticleView(mainActivity);
        articleView.setArticle(getArticles(groupPosition).get(childPosition));
        return articleView.getView();
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
